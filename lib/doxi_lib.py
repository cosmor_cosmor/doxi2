#-*- coding: utf-8 -*-
#
#
# 
# v 0.4.0.0.4 - 2013-07-03


import os, sys, ConfigParser, time, glob, random, hashlib

config = ConfigParser.RawConfigParser()
if os.path.isfile("doxi.conf"):
    dc = "doxi.conf"
else:
    dc = "../doxi.conf"
config.read(dc)
nginx_conf_path = config.get('global', 'nginx_conf_path').strip()
nginx_bin       = config.get('global', 'nginx_bin').strip()
nginx_restart   = config.get('global', 'nginx_restart').strip()
doxi_rules_dir  = config.get('global', 'doxi_rules_dir' ).strip() 
if doxi_rules_dir[0] != "/":
    # ncheck for relative/absolute path
    doxi_rules_dir = "%s/%s" % (nginx_conf_path, doxi_rules_dir)
rollback_dir    = config.get('global', 'rollback_dir').strip()
stats_dir       = config.get('global', 'stats_dir').strip()

time_ranges = {
    #desig   steps          name         shortcut       chart_steps
    "h"    : (3600,         "1hr",      "lasthour",     48  ), # hours
    "d"    : (86400,        "24hrs",    "lastday",      14   ), # days 86400
    "w"    : (604800,       "7days",    "lastweek",     8   ), # weeks 604800
    "m"    : (2592000,      "30days",   "lastmonth",    12  ), # months 2592000
    "y"    : (31536000,     "1yr",      "lastyear",     5   ), # years
    "a"    : (3153600000,   "All",      "all",          0   ), #all # 2592000

}

try:

    nx_util_dir   = config.get('nx_util', 'nx_util_dir').strip()
    nx_util_conf = "%s/nx_util.conf" % nx_util_dir.strip()
except:
    print """

ERROR

you need to adjust your doxi.conf; [naxsi-ui] is obsolte, 
use [nx_util] instead

see Changelog for v0.4 and doxi.conf.template
    
    """
    sys.exit(2)


try:
    rep_list_count = int(config.get('dx_report', 'reputation_list_count').strip())
except:
    print """

[i] rep_list_count defaults to 3, 
see Changelog for v0.4 and doxi.conf.template
    
    """
    rep_list_count = 3



# for dx-result 
tmpdb=":memory:"

date_stamp = "%s.%s" % (time.strftime("%Y-%d-%m", time.localtime(time.time())), int(time.time()))
date_time  = "%s" % (time.strftime("%F - %H:%M", time.localtime(time.time())))

tcreate = "create table if not exists tmptbl (id INTEGER PRIMARY KEY AUTOINCREMENT, count INTEGER, sid INTEGER)"



def parse_new_doxi_rules():
    
    new_sigs = {}
   
    known_sigs = "%s/know_sigs" % stats_dir
    if not os.path.isdir(known_sigs):
        try:
            os.makedirs(known_sigs)
        except:
            return(1000)

    
    
    doxi_rulesets = glob.glob("doxi-rules/*.rules")
    
    global_sigs = {}
    
    for ruleset in doxi_rulesets:
        print "checking %s" % ruleset
        sigs = parse_rulesfile(ruleset)
    
        
        for sig in sigs:
            global_sigs[sig] = sigs[sig]
    
    
    
    for sid, stro in sorted(global_sigs.iteritems()):
    #    print sid
    #    print stro
    #    print global_sigs[sid]
        sid_path = "%s/%s" % (known_sigs, sid)
        if not os.path.isfile(sid_path):
            new_sigs[sid] = stro
            f = open(sid_path, "a")
            f.write("""#
# sid     : %s
# created : %s

%s 
            """ % (sid, date_stamp, stro[4]))
            f.close()
        else:
            #print "[i] %10s is know" % sid
            continue

    if len(new_sigs) == 0:
        return(0)
    print "[+] new sigs:"
    for sig in sorted(new_sigs):
        print "%10s :: %-20s :: %s" % (sig, new_sigs[sig][5], new_sigs[sig][0])

def parse_doxi_rules():
    
    doxi_rulesets = glob.glob("%s/*.rules" % doxi_rules_dir)
    
    global_sigs = {}
    
    for ruleset in doxi_rulesets:
        #print "checking %s" % ruleset
        sigs = parse_rulesfile(ruleset)
    
        
        for sig in sigs:
            global_sigs[sig] = sigs[sig]
    
    
    return(global_sigs)


def random_string(l=128):
    """
    takes l(length) of random_chars to return;
    min(l) = 16
    max(l) = 4096
    """
    ascii="""qwertzuiopasdfghjklyxcvbnmQWERTZUIOPASDGHJKLYXCVBNM,.-#+ß0987654321;:_'\ /"*?=)(/&%$§!\}][{@<>|"""

    w = ""
    for x in range(1,100):
        y = (random.sample(ascii, 12))
        random.shuffle((y))
        w += "".join(y)
    if len(w) < 1000:
        w = ascii 
        for g in range(1,100):
            w += random.shuffle("".join(w))
    
    try:
        int(l)
    except:
        l = 16

    if l < 16:
        l = 16
    elif l > 4096:
        l = 1024
    r = hashlib.sha512(w).hexdigest()
    while len(r) < (10 * l):
        random.shuffle(r.split())
        r += "".join(r)
        
    return("".join(random.sample(r,l)))
    
    
    

def parse_rulesfile(rfile):
    """
    parses a given rulefile and returns a sorted dictionary with
    
    sigs = {
    ...
    idx : (msg, searchstring, mz, score, full_line_from_file, ruleset), 
    ...
    }
    
    """

    if not os.path.isfile(rfile):
        return(0)
    
    lc = 0
    rc = 0
    
    sigs = {}
    
    for line in open(rfile, "r").xreadlines():
        msg = mz = score = rid = 0
        line = line.strip()
        #print line
        lc += 1
        if len(line) < 15:
            continue
        if line[0] == "#":
            continue
        if not line.find("MainRule") > -1:
            continue


        try:
            rid = line.split("id:")[1].split()[0].replace("\"", "").replace(";", "").strip()
        except:
            continue
        try:
            int(rid)
        except:
            print "[-] ERROR - ID should be an integer, found: %s" % rid
            continue

        try:
            msg = line.split("msg:")[1].split("\"")[0].strip()
        except:
            print "[-] ERROR - no msg: - identifier found"
            continue

        if line.find("str:") > -1:
            srchp = "str:"
        elif line.find("rx:") > -1:
            srchp = "rx:"
        else:
            continue

        try:
            srch = "%s%s" % (srchp, line.split(srchp)[1].split("\"")[0].strip())
        except:
            print "[-] ERROR - no str:/rx: - identifier found"
            continue

        try:
            mz = line.split("mz:")[1].split("\"")[0].strip()
        except:
            print "[-] ERROR - no mz: - identifier found"
            continue

        try:
            score = line.split("s:")[1].split("\"")[0].strip()
        except:
            print "[-] ERROR - no score: - identifier found"
            continue

        sigs[rid] = (msg, srch, mz, score, line, rfile.split("/")[-1])
        
        #print rid

    return(sigs)
        
#MainRule "string:/sftp-config.json" "msg:DN WEB_SERVER SFTP-config-file access" "mz:URL|BODY" "s:$ATTACK:8,$UWA:8" id:42000084 ; 


