
<hr>
<a name="hints"></a>
<div align="left" markdown="1">

## Search-Hints

### Search-Terms

** Search (terms from naxsi_db) **

- you can use each search_term stand_alone or in combination 

  - rule_id / sid / id
    - id-search
    - MUST match; no regexes possible
  - peer_ip / ip
    - select peer_ip / ip that generated events
    - string-search 
    - behaves like SQL_LIKE: ip = 192.168*
  - mz / zone
    - select matching-zone
    - string-search (LIEK)
    - e.g *php -> find all that ends with php; *php* finds all strings that contains php
  - url 
    - select part or full url
    - string-search (LIEK)





### Special Searches

- uniq | search_term | 
- compare | host=x,y
- detail | host = x : ip=y,z


### RegExes

  - you **cant** regex rule_ids, only ip/host/url/mz
  - you can use some kind of regex

~~~

$srch = *term*; 
  - * gets translated into .*, a
  - . gets translated into \. for regexing ips
  - ^ Marks the beginning of a search_term, eg ip
  - $ Marks the ending of a search_term
  

~~~

### Examples

- select all
  
~~~
    ip = *
~~~

- select all ips that starts with 8
  
~~~
    ip = ^8*
~~~

- select all ips that ends with 14
  
~~~
    ip = *14$

~~~


- select a certain rule_id with a certain ip

~~~
    id = sid & ip = attacker_ip 
~~~

- select all events from a certain ip, except a given rule_id
~~~
    ip = attacker_ip & rule_id = !exclude_sid     
~~~

- select all events from rule_id 100 and all ips, starting with 202.
  
~~~
    id = 1000 & ip = 202.*
~~~

- select all events from 2 ips, except a given rule_id
~~~
    peer_ip = attacker_ip, attacker_ip2 ! rule_id = exclude_sid    
~~~

- display uniq_urls / uniq hosts etc pp

~~~

    uniq | url
    uniq | host
    uniq | mz
    uniq | ip
    uniq | sensor_id 
    
~~~




### TimeRange

- defaults to 1 week / 7 days

~~~
    1h      -> 1 Hour 
    2d      -> 2 days
    3w      -> 3 weeks
    4m      -> 4 months
    
    1d-3d   -> everything from 1d ago until 3 days ago
    
~~~

    

</div>
