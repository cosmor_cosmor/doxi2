

<div align="left" markdown="1">
### DX-Console - Features

- centralised display of distributed naxsi-installations

#### Dashboard 

- dashboard for quick overview
- graphics and cumulated results for 24hrs/7d/30d 
- comprehensive sensor+console-status-display (tbf)


#### latest events

- latest events (kinda livelog) 
- marking of new events

#### Search

- standalone search for ips/naxsi-ids/hosts etc, e.g ip = x.x.x.x 
- combined search like host = www.example.com & id = 1042 
- free adjustable timerange-based filter, e.g. 1d/7d/42w, but also 7d-14d
- mark results as false-positives / delete from display
- saved searches (saving search-terms and results) -> generating templates 
- export searches and false-positives-lists
- uniq/detail/compare - searches 
- graphs for timerange-based search


#### Analysis

- Searches
- rules_id/host-based whitelist_generation
- detailed analysis for all parts of an alert, standalone or in combination


#### Alerts and Agents

- alerts on new found url/ip/host 
- running agents that alert when given threshould are met, e.g. attacks/day  (tbf)


#### IP-Reputation 
- ip-reputation-control (makes only sense with a lot of sensors, /me thinx


#### Admin

- user-administration
- clean caches

#### API

- api-interface for agents (think nagios) (tbd)


#### Misc

- audit your own ip; usefull when pentesting a site
- flask-based application
- modified nx_util
    - mongo-db for storing event-data
    - skip known events, thus enabling a shot-term cronjob for nx_util 
- --fancy webX.0 - flat njustyle webscale js-only based interface-- bootstrap-based interface 

- [Screenshots](http://doxi-news.blogspot.de/2013/08/dx-console-central-interface-to.html)


<hr>

- tbd: to be done / not yet implemented
- tbf: to be finished / implemented bot not yet fully tested/funtional

</div>
