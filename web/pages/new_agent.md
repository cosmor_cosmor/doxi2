
<div align="left" markdown="1">

## Create New Agent


please note: this is very basic now, please read the [docs](/doc/agents)

<form action="/do/new_agent/" method="POST">
<table cellpadding="10">

<tr><td>Agent_Name</td><td><input type="text" name="aname"></td><td>[+] 3 chars minimum</td></tr>
<tr><td>Set Host</td><td><select name="host"><option value="all" selected>All</option>%s</select></td><td>*</td></tr>
<tr><td>Set ID</td><td><input type="text" name="sid"></td><td>leave empty for all, separate multiple choices by spaces</td></tr>
<tr><td>Set IP</td><td><input type="text" name="ip"></td><td>leave empty for all, separate multiple choices  by spaces</td></tr>
<tr><td>Set Intervall</td><td>
    <select name="interval">
        <option value="5m">5 minutes</option>
        <option value="1h">1 hour</option>
        <option selected value="24h">24 hours</option>        
    </select>
    </td><td>*</td></tr>
<tr><td>Set Count &gt;=</td><td><input type="text" name="count"></td><td>*</td></tr>
<tr><td>Mail on Alert</td><td>
    <input type="radio" name="mail_alerts" value="1" checked>Yes<br>
    <input type="radio" name="mail_alerts" value="0">No<br></td>
<td></td></tr>

<tr><td></td><td><input type="submit" class="btn btn-primary" value="save"></td><td></td></tr>



</table>

*) -> needed values

</div>
